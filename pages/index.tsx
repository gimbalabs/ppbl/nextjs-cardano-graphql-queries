import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import ExampleGraphQLQuery from '../components/exampleGraphQLQuery'
import styles from '../styles/Home.module.css'

const Home: NextPage = () => {
  return (
    <>
      <h1>Hello World</h1>
      <ExampleGraphQLQuery />
    </>
  )
}

export default Home
