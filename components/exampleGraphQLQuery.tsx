import { useQuery, gql } from "@apollo/client";

const NEWQUERY = gql`
    query ContractQuery($contractAddress: String!){
        utxos(where: {address: {_eq: $contractAddress}}){
            transaction {
                hash
            }
            index
            value
        }
    }
`

export default function ExampleGraphQLQuery() {

    const { data, loading, error } = useQuery(NEWQUERY, {
        variables: {
            contractAddress: "addr_test1wrfc4hux6ffmqycnzzv6kfk2luqxeskgdn4m6l6z5txzcuqtw0r3d"
        }
    });

    return (
        <>
            <h1>Make a query!</h1>
            <h2>Query Results</h2>
            <p>{JSON.stringify(data)}</p>
            {data.utxos.map((apples: any) => <p><pre>{apples.transaction.hash}#{apples.index}</pre> has {apples.value} lovelace</p>)}
        </>
    )
}